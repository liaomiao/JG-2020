### 这是第三组项目

#### 任务分工：

##### 运行项目需要环境: Aache + PHP5.3 + MYSQL 

##### 编辑器推荐：PhpStorm （集成 PHP 和 git  等运行环境，方便。。）

master为主枝（运行中的项目，请勿直接保存上传到此），所有测试项目均先上传到dev分支，经过测试，没问题后，在合并到主枝。

#### 项目组员：

----------廖淼：试用页面，及爬虫抓取，存取数据库API接口

----------徐路金：主页页面，及爬虫抓取，存取数据库API接口

----------李深：新品页面

----------余宏彪：页头

----------白元：页脚

----------卢邦云：全部代码 (大白天的...)

---

#### 项目规范：

[![Total Downloads](https://poser.pugx.org/topthink/think/downloads)](https://packagist.org/packages/topthink/think)
[![Latest Stable Version](https://poser.pugx.org/topthink/think/v/stable)](https://packagist.org/packages/topthink/think)
[![Latest Unstable Version](https://poser.pugx.org/topthink/think/v/unstable)](https://packagist.org/packages/topthink/think)
[![License](https://poser.pugx.org/topthink/think/license)](https://packagist.org/packages/topthink/think)

##### 注意如下规范:

​		请各位组员按照以下规范命名，防止冲突。

```html
文件目录:命名规范 安装模块名 进行起码
	文件结构目录的解释
	代码中类名规范
		把所有公共的样式先写好(common.css)
		每个模块: 每个模块的布局ID (命名方式按照模块的名字起名) 可以按模块进行划分文件(js , css)
			#home .banner
```

##### 目录和文件

- 目录不强制规范，驼峰和小写+下划线模式均支持；
- 类库、函数文件统一以`.php`为后缀；
- 类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
- 类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

##### 函数和类、属性命名

- 类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserType`，默认不需要添加后缀，例如`UserController`应该直接命名为`User`；
- 函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 `get_client_ip`；
- 方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
- 属性的命名使用驼峰法，并且首字母小写，例如 `tableName`、`instance`；
- 以双下划线“__”打头的函数或方法作为魔法方法，例如 `__call` 和 `__autoload`；

##### 常量和配置

- 常量以大写字母和下划线命名，例如 `APP_PATH`和 `THINK_PATH`；
- 配置参数以小写字母和下划线命名，例如 `url_route_on` 和`url_convert`；

##### 数据表和字段

- 数据表和字段采用小写加下划线方式命名，并注意字段名不要以下划线开头，例如 `think_user` 表和 `user_name`字段，不建议使用驼峰和中文作为数据表字段命名。

---

### 初始目录结构：(文件目录结构详解)

```
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  ├─module_name        模块目录
│  │  ├─config.php      模块配置文件
│  │  ├─common.php      模块函数文件
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─view            视图目录
│  │  └─ ...            更多类库目录
│  │
│  ├─command.php        命令行工具配置文件
│  ├─common.php         公共函数文件
│  ├─config.php         公共配置文件
│  ├─route.php          路由配置文件
│  ├─tags.php           应用行为扩展定义文件
│  └─database.php       数据库配置文件
│
├─public                WEB目录（对外访问目录）
│  ├─static				静态资源文件
│  │  ├─css           	css样式
│  │  ├─fonts     		字体
│  │  ├─images			图片
│  │  ├─js				js脚本
│  │  ├─less			less预处理语言
│  │  └─sql				数据库表
│  │
│  ├─index.php          入口文件
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─thinkphp              框架系统目录
│  ├─lang               语言文件目录
│  ├─library            框架类库目录
│  │  ├─think           Think类库包目录
│  │  └─traits          系统Trait目录
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  ├─phpunit.xml        phpunit配置文件
│  └─start.php          框架入口文件
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
```

___

### 工作日志：(请各位组员完成项目后添加日志，用于管理！)

###### 			徐路金 2020年01月06日15点38分 测试上传了一次

###### 2020年1月6日16:25:53 卢邦云(老师)拒绝带领3组成员走向辉煌。。。

###### 徐路金 2020年01月07日11点49分 调好api接口并且插入了爬虫插件（测试正常）

###### 徐路金 2020年01月07日17点32分 把首页，使用，新品的数据爬好并存入了数据库（正常）

​		在c盘配好域名解析

###### 2020年1月7日17:36:51 更新了爬虫抓取及数据库表单

###### 2020年1月8日15:11:41 更新了试用页头部

###### 2020年1月8日17:45:52 更新了试用页的数据接口API（数据转化OK）

###### 2020年1月8日17:53:24 更新了新品页面的静态代码

###### 2020年1月8日17:53:48 更新了首页页面的静态代码（及首页数据接口，转化OK）

###### 2020年1月9日14:35:38 更新了链接相通

###### 2020年1月9日16:39:30 更新了试用页的前端渲染静态页面（部分数据导入成功）

###### 2020年1月10日20:55:11 更新了试用页及通用页脚，用的时候请引入好CSS

###### 2020年1月10日21:04:31 更新了首页及新品页的完整性

###### 2020年1月11日16:11:53 更新了新品页的数据库及动态数据页面

###### 徐路金 2020年01月12日11点27分 把布局懒加载添加了进去

###### 徐路金 2020年01月13日11点19分 把登录模态框加了进去