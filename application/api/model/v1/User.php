<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 16:51
 */

namespace app\api\model\v1;
use think\Db;
use think\Request;

class User
{
    static function login($params){
        $params['PASSWORD'] = md5('WT18'.md5($params['PASSWORD']));
        //执行登录查询操作
        return Db::table("users")->field("USERNAME,USERNICK,UID")->where($params)->select();
    }
    static function recordToken($token,$uid){
        $params['TOKEN'] = $token;
        $params['UID'] = $uid;
        $request = Request::instance();
        $params['CREATE_IP'] = $request->ip();
        $params['EXPIRES']=date('Y-m-d H:i:s',time() + 30*24*60*60);
        Db::table("user_login_tokens")->insert($params);
    }
}