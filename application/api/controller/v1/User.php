<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 16:03
 */

namespace app\api\controller\v1;
use app\api\controller\v1\common\Common;
use app\api\model\v1\User as userModel;

class User extends Common
{
    //登录接口
    public function login(){
        //使用模型进行数据库的操作
        $params = input("post.");//input 一个内置的助手函数用户接受各种传递的参数
        //此处还应该需要做一次表单验证 ... (略去)
        $res = userModel::login($params);
        if(!$res){
            Common::toJson(0,"登录失败!");
        }else{
            //生成token
            $token = md5($res[0]['UID'].time().mt_rand(10000,99999));
            //将颁发过的token要记录到数据库中
            $res[0]['token'] = $token;
            userModel::recordToken($token,$res[0]['UID']);
            Common::toJson(200,"登录成功!",$res);
        }
    }

    //注册接口
    function register(){
        
    }
}