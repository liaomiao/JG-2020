<?php
/**
 * Created by PhpStorm.
 * User: 小徐
 * Date: 2020/1/8
 * Time: 18:21
 */

namespace app\api\controller\v1;

use think\Db;
class NewThing
{
    public function index(){
        $data = Db::name('new_thing_page')->limit(48)->select();
        $database = [
            'code'=>200,
            'msg'=>"success",
            'data'=>[$data],
        ];

        $json = json_encode($database,JSON_UNESCAPED_UNICODE);
        echo $json;
    }
}