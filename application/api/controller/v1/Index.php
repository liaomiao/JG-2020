<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 13:57
 */

namespace app\api\controller\v1;

//use app\api\controller\v1\Index as IndexModel;

use think\Db;

class Index
{
    public function index(){

        $data = Db::table('home_page_article')->select();
        print_r(json_encode($data,JSON_UNESCAPED_UNICODE));


    }
}