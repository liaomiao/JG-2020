<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 16:11
 */


namespace app\api\controller\v1\common;

//use app\api\controller\behavior\Cors;
class Common
{
    public function _empty(){
        $json = [
            'code'=>404,
            'msg'=>"Access error",
        ];
        return json($json);
    }

    //一个用法返回json数据格式的函数
    static function toJson($code=200,$msg="请求数据成功~",$data=[],$len=null){

        $params = [
            'code'=>$code,
            'msg'=>$msg,
        ];
        if(count($data) > 0){
            $params['data'] = $data;
            if($len){
                $params['total'] = $len;
            }
        }
        $json = json_encode($params,JSON_UNESCAPED_UNICODE);
        //清空之前所有的输出,避免返回了一些没用的东西
//        ob_clean();
        echo $json;
    }
}