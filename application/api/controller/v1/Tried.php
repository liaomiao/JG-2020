<?php
/**
 * Created by PhpStorm.
 * User: 小徐
 * Date: 2020/1/7
 * Time: 11:18
 */

namespace app\api\controller\v1;

use think\Db;

class Tried
{
    public function index(){
        $data = Db::name('trial')->limit(20)->select();
        $database = [
            'code'=>200,
            'msg'=>"success",
            'data'=>[$data],
        ];

        $json = json_encode($database,JSON_UNESCAPED_UNICODE);
        echo $json;
    }
}