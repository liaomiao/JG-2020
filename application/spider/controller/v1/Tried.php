<?php
/**
 * Created by PhpStorm.
 * User: 小徐
 * Date: 2020/1/7
 * Time: 11:18
 */

namespace app\spider\controller\v1;
//使用公用方法
use app\api\controller\v1\common\Common;


//使用爬虫
use QL\QueryList;
use think\Db;


//使用模型里面的方法
use app\spider\model\v1\Tried as TriedModels;
class tried extends Common
{
    function index()
    {
        //爬虫规制
        for ($page = 0; $page < 1; $page++) {
            $params = [
                'title' => ['.e-title', 'text'],
                'imgsrc' => ['.img-box>img', 'src'],
                'imgurl' => ['.event-show-card-warp a', 'href']
            ];

            $ql = QueryList::Query("http://www.jiguo.com/event/index/page/'.$page.'/act/newthing.html", $params);

            $data = $ql->data;

//                print_r($data);

            TriedModels::Index($data);
        }

//        return json($data,JSON_UNESCAPED_UNICODE);
    }

    public function test()
    {

        //爬取的内容规则
        //爬取试用页面
        $params = [
            'title' => [".e-title", "text"],
            'imgUrl' => ['.img-box img', "src"],
            'eTag' => ['div.e-tag-list', "html"],
            'peopleNum' => ['font.red', "text"],
            'peopleNumfr' => ['.e-query', "text"],
            'otherTag' => ['.e-item-hide', "html"]
        ];
        for ($j = 0; $j < 141; $j++) {
            $ql = QueryList::Query('http://www.jiguo.com/event/index/page/' . $j . '/act/index.html', $params)->getData(function ($item) {
                $item['otherTag'] = QueryList::Query($item['otherTag'], [
                    'otherTag' => ['span', 'text']
                ])->data;
                $item['eTag'] = QueryList::Query($item['eTag'], [
                    'eTag' => ['span', 'text']
                ])->data;

                //以下将专享试用标签数组扁平化处理
                $return = [];
                array_walk_recursive($item['eTag'], function ($x, $index) use (&$return) {
                    $return[] = $x;
                    return $return;
                });
                $item['eTag'] = $return;
                foreach ($item['eTag'] as $k => $v) {
//                print_r($k);
                    $item['eTag' . $k] = $v;
                }
                unset($item['eTag']);

                //以下将hover标签数组扁平化处理
                $return2 = [];
                array_walk_recursive($item['otherTag'], function ($s, $index) use (&$return2) {
                    $return2[] = $s;
                    return $return2;
                });
                $item['otherTag'] = $return2;
                foreach ($item['otherTag'] as $k => $v) {
//                print_r($k);
                    $item['otherTag' . $k] = $v;
                }
                unset($item['otherTag']);

                return $item;
            });

//           print_r($ql);
//        $num=Db::table("trial")->insertAll($ql);
            for ($i = 0; $i < count($ql); $i++) {
                Db::table("trial")->insert($ql[$i]);
            }
        }
}
}
