<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 16:18
 */

namespace app\api\controller\v1\common;


class Error
{
    public function index(){
        $json = [
            'code'=>10000,
            'msg'=>"当前接口的访问方式不对,请确定API!",
        ];
        return json($json);
    }
}