<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/12/30
 * Time: 13:57
 */

namespace app\spider\controller\v1;
use app\api\controller\v1\common\Common;

use QL\QueryList;

//使用模型里面的方法
use app\spider\model\v1\Index as IndexModel;
class Index extends Common
{
    function index(){
        for( $page=0;$page<1;$page++ ){
            $params = [
                'title'=>['.desc-box>.d-title','text'],
                'imgsrc'=>['.choice-img-box>img','src'],
            ];

            $ql = QueryList::Query("http://www.jiguo.com/index/index/spm/smpc.content.content.11.1546250500652ZgdSGZS/page/".$page.".html",$params);

            $data = $ql->data;

        IndexModel::index($data);
//            print_r($data);
        }

    }
}