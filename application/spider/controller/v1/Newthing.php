<?php
/**
 * Created by PhpStorm.
 * User: 小徐
 * Date: 2020/1/7
 * Time: 15:09
 */

namespace app\spider\controller\v1;
//使用公用方法
use app\api\controller\v1\common\Common;

//使用爬虫插件方法
use QL\QueryList;
use think\Db;
//使用模型里面的方法
use app\spider\model\v1\NewThing as NewThingModel;

class Newthing extends Common
{
    function index(){
        //爬取新品页面
        $params2 = [
            'title' => [".d-title", "text"],
            'imgUrl' => ['.choice-img-box img', "src"],
            'dNum' => ['.d-number', "html"]
        ];

        for ($j = 0; $j < 175; $j++) {
            $ql2 = QueryList::Query('http://www.jiguo.com/product/index/type/2/page/' . $j . '.html',   $params2)->getData(function ($item) {
                $item['dNum'] = QueryList::Query($item['dNum'], [
                    'dNum' => ['span', 'text']
                ])->data;

                //以下将标签数组扁平化处理
                $return = [];
                array_walk_recursive($item['dNum'], function ($x, $index) use (&$return) {
                    $return[] = $x;
                    return $return;
                });
                $item['dNum'] = $return;
                foreach ($item['dNum'] as $k => $v) {
                print_r($k);
                    $item['dNum' . $k] = $v;
                }
                unset($item['dNum']);
                return $item;
            });
            print_r($ql2);
            for ($i = 0; $i < count($ql2); $i++) {
                Db::table("new_thing_page")->insert($ql2[$i]);
            }
        }

        //使用QueryList 类中静态方法(不需要实例化可以直接使用的方法)
//        爬取轮播图
//        $params3 = [
//            'imgUrl' => ['.banner-img-bg img', "src"],
//            'url' => ['.banner-img-bg a', "href"],
//        ];
//            $ql3 = QueryList::Query('http://www.jiguo.com/index/index.html', $params3)->data;
//            print_r($ql3);
//            for ($i = 0; $i < count($ql3); $i++) {
//                Db::table("banner")->insert($ql3[$i]);
//            }


//        return json($data,JSON_UNESCAPED_UNICODE);
    }
}