<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

//绑定域名部署 - api
Route::domain("api",function(){
    //配置一个动态路由
    Route::rule("/","api/v1.Index/index");
    //配置user模块路由
    Route::rule("/user/:function","api/v1.User/:function");
    Route::rule("/user/","api/v1.Common.Error/index");
    //配置试用模块路由
    Route::rule("/tried/[:function]","api/v1.tried/:function");
    Route::rule("/tried/","api/v1.Common.Error/index");
    //配置新品模块路由
    Route::rule("/newthing/[:function]","api/v1.NewThing/:function");
    Route::rule("/newthing/","api/v1.Common.Error/index");
    //配置生成图片验证码的模块路由
    Route::rule("/verifycode/[:function]","api/v1.VerifyCode/:function");
    Route::rule("/verifycode/","api/v1.Common.Error/index");
    //配置验证图片验证码是否正确
    Route::rule("/verifycode/[:function]","api/v1.VerifyCode/:function");
    Route::rule("/verifycode/","api/v1.Common.Error/index");
});

//绑定域名部署 - spider
Route::domain("spider",function(){
    //配置一个动态路由
    Route::rule("/","spider/v1.Index/index");
    //配置试用模块路由
    Route::rule("/tried/[:function]","spider/v1.tried/:function");
    Route::rule("/tried/","spider/v1.Common.Error/index");
    Route::rule("/tried/","spider/controller.v1.Common.tried/:function");
    //配置新品模块路由
    Route::rule("/newthing/[:function]","spider/v1.newthing/:function");
    Route::rule("/newthing/","spider/v1.Common.Error/index");
});




/***前台模块路由**/
Route::rule("/","index/v1.index/index");
//前台主页
Route::rule("/index","index/v1.index/index");
//前台试用页
Route::rule("/tried","index/v1.tried/index");
//前台新品页
Route::rule("/newthing","index/v1.newthing/index");
//前台用户页
Route::rule("/login","index/v1.user/login");
Route::rule("/register","index/v1.user/register");