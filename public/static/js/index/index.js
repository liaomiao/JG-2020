/**************轮播图js********************/
var carousel = document.getElementsByClassName("carousel")[0];//图片容器
var slideImg = document.getElementsByClassName("slide-img")[0];//图片容器
var slideBtn = document.getElementsByClassName("slide-btn")[0];//按钮容器
var slideImgLi = slideImg.children;//图片容器的Li
var slideBtnLi = slideBtn.children;//按钮容器的Li
var prev = document.getElementsByClassName("prev")[0];//上一页
var next = document.getElementsByClassName("next")[0];//下一页
var carouselControl = document.getElementsByClassName("carousel-control")[0];//上下翻页控件

var len = slideImgLi.length;//长度
var timer = null;//定时器
var index = -1;//图片的索引位置


//定义一个自动轮播的函数
function autoplay(){
    index++;
    timer = setTimeout(autoplay,1500);
    // console.log(index);
    moveImg();
}
autoplay();

//定义一个图片切换的函数
function moveImg(){
    //重置样式
    if( index>=len )index=0;
    if( index<0 )index = len-1;
    for( var i=0;i<len;i++ ){
        slideImgLi[i].classList.remove("active");
        slideBtnLi[i].classList.remove("active");
    }

    slideImgLi[index].classList.add("active");
    slideBtnLi[index].classList.add("active");
}

//鼠标移入暂停移出继续
carousel.onmouseenter = function(){
    clearTimeout(timer);
}
carousel.onmouseleave = function(){
    autoplay();
}

//前后翻页
prev.onclick = function(){
    index--;
    moveImg();
}
next.onclick = function(){
    moveImg();
    index++;
    // console.log("下一页");
}


//精选推荐下面的小盒子的左边的左边距为0
var eachRecommend = document.getElementsByClassName("each-recommend")[0];
var eachRecommendChild = eachRecommend.children;

// console.log(eachRecommendChild);
for( var i=0;i<eachRecommendChild.length;i++ ){
    if( i%3==0 ){
        eachRecommendChild[i].style.marginLeft = 0;
    }
}

axios.get('http://api.jg.com/')
.then(function(res){
    // console.log(res.data);
    var reverseArr = res.data.reverse();
    // console.log(reverseArr);
    var htmlStr = "";
    for( var i=0;i<reverseArr.length;i++ ){
        htmlStr += ' <div> <a class="each-recommend-img" href=""> <img data-src="https:'+reverseArr[i].imgsrc+'" src="" alt=""> </a> <div class="each-recommend-items"> <a href="" class="each-recommend-descript">'+reverseArr[i].title+'</a> <p class="avatarandnick"> <a href="" class="avatar"><img src="https:'+reverseArr[i].imgsrc+'" alt=""></a> <a href="" class="nick">永恒的金色年华</a> </p> <a href="" class="past-time">3小时前</a> </div> </div>'
    }
    eachRecommend.innerHTML = htmlStr;

    
})
.catch(function(err){
    // console.log(err);
})