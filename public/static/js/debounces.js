//定义一个防抖的函数
function debounce(handler,inter){
    var timer = null;

    handler();
    return function (){
        if( timer!==null )clearTimeout(timer);
        timer = setTimeout(handler,timer);
    }
}

function handler(){
    var clientW = window.innerWidth;

    var htmlFontSize = document.documentElement.style.fontSize;
    document.documentElement.style.cssText = 'font-size:'+clientW/1440 * 100+'px;';
}

window.addEventListener('resize',debounce(handler,300));
