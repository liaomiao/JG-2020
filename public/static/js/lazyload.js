setTimeout(function(){
    start()//没滚动也要加载出当前显示在可视窗的图片
},2000);
var clock;//函数节流（定时器clock）
$(window).on("scroll",function(){
    if( clock ){
        clearTimeout( clock )
    };
    clock = setTimeout(function(){
        start()//在外面定义一个加载图片的函数
    },2000);
});

//定义一个加载图片的函数
function start(){
    $("img[src='']").each(function(){//src属性为空的把data-src属性值加入进去
        if( isShow( $(this) ) ){
            loadImg( $(this) );
            $(this).parent().css({//图片加载完成之后清除加载动画
                background:"url('')"
            })
            $(this).parents(".slide").css({//图片加载完成之后清除加载动画
                background:"url('')"
            })
        };
    });
};

//判断图片是否出现在视窗的函数

function isShow($node){
    return $node.offset().top <= $(window).height()+$(window).scrollTop();
    //如果图片距离网页最顶部的距离小于等于网页可视高度加上网页滚动距顶部的距离则判断图片出现了
};

//加载图片的函数，也就吧data-src属性给到src属性
function loadImg($img){
    $img.attr('src',$img.attr('data-src'));
};
