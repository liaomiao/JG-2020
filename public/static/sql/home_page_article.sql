-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2020 �?01 �?11 �?16:20
-- 服务器版本: 5.5.47
-- PHP 版本: 5.6.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `jg-2020`
--

-- --------------------------------------------------------

--
-- 表的结构 `home_page_article`
--

CREATE TABLE IF NOT EXISTS `home_page_article` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '流水ID',
  `title` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT '图片的标题',
  `imgsrc` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT '图片的地址',
  `imgurl` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT '图片跳转地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=451 ;

--
-- 转存表中的数据 `home_page_article`
--

INSERT INTO `home_page_article` (`id`, `title`, `imgsrc`, `imgurl`) VALUES
(450, '百人共绘跨年巨制，绽放影像艺术大展，华为MatePad Pro科技赋能艺术创造', '//s2.jiguo.com/fafb3049-7618-4fe6-a488-ab66fa3a2896/640?imageView2/1/w/520/h/320/q/100', ''),
(449, '内衣印他名字价格翻倍卖…这个男人能解锁女人最性感一面！', '//s2.jiguo.com/3a1ec376-41fb-4d3b-8e7f-3174c8454b47/640?imageView2/1/w/520/h/320/q/100', ''),
(447, '10年不用换新机！5G变身“省钱”神器，老掉渣电子废品迎来第二春......', '//s2.jiguo.com/baeef08a-5de3-4c33-8417-4064346a2860/640?imageView2/1/w/520/h/320/q/100', ''),
(448, '科技赋能美学教育，华为MatePad Pro大师班开讲', '//s2.jiguo.com/832fecdc-9eee-4d7b-8cfc-38468e5756d5/640?imageView2/1/w/520/h/320/q/100', ''),
(446, '翻盖机文艺复兴！诺基亚、moto都参战，随手一翻都是回忆……', '//s2.jiguo.com/53140de5-b0c1-4bb4-b235-bee9a83b64fc/640?imageView2/1/w/520/h/320/q/100', ''),
(445, '打造最“燃”画面 ！GoPro灯光选配组件即将开售 ，仅售398元', '//s2.jiguo.com/3d44fac0-78cd-4ba2-b24a-8154b4baeda1/640?imageView2/1/w/520/h/320/q/100', ''),
(444, '「体验」能听懂方言，还能秒变随身翻译！这款智能鼠标让工作效率翻倍！', '//s2.jiguo.com/2440fc84-f300-474c-a589-f829e81dbb44/640?imageView2/1/w/520/h/320/q/100', ''),
(442, '售价最低199！鼠标也玩电动升降，化身会办公的“变形金刚”', '//s2.jiguo.com/8af61cf1-925b-4618-9227-5a96c93c1581/640?imageView2/1/w/520/h/320/q/100', ''),
(443, '浑身“肌肉”！4K投影竟满身黑科技，大片观感爽到爆炸......', '//s2.jiguo.com/5656b4ba-f8da-4e14-a7f6-460b18887d9f/640?imageView2/1/w/520/h/320/q/100', ''),
(441, '「体验」美女博主竟用ta记录旅程瞬间，比拍照更有创意，手感简直上瘾！', '//s2.jiguo.com/1ee4bdcd-7478-42e8-8e71-a0d31d9cfe70/640?imageView2/1/w/520/h/320/q/100', ''),
(440, '组建5G生态必备！从“智商税”到全场景，华为5G随行WiFi Pro动手玩', '//s2.jiguo.com/9ad86b58-0f55-48e0-a7d8-9487c8ac5d30/640?imageView2/1/w/520/h/320/q/100', ''),
(439, '日销10个亿国内“最潮腐败”购物地！土豪明星的最爱，花50万都要排队......', '//s2.jiguo.com/5e57df4d-05f9-4638-a23b-1fed737128cb/640?imageView2/1/w/520/h/320/q/100', ''),
(438, '60倍变焦+暴走5G，今年过年能不能制霸朋友圈就靠它了', '//s2.jiguo.com/c3bab46e-d8bd-43df-978d-87d19f20e43f/640?imageView2/1/w/520/h/320/q/100', ''),
(437, '「体验」海信双屏阅读手机A6L！墨彩双屏随心切换，纸质阅读硬核护眼', '//s2.jiguo.com/cde5de33-b9c6-4e47-bcfc-4a5a8ac27a0c/640?imageView2/1/w/520/h/320/q/100', ''),
(435, '干翻手机云台？Reno 3 Pro评测:终于知道5G手机能干啥了……', '//s2.jiguo.com/9cf31d59-91f7-434a-9178-6698e08c96e1/640?imageView2/1/w/520/h/320/q/100', ''),
(436, '年度最轻薄双模5G手机！OPPO Reno3系列发布，售价3399元起', '//s2.jiguo.com/aad91973-ecc2-440a-b7df-8e19791879ea/640?imageView2/1/w/520/h/320/q/100', ''),
(434, '「体验」23种方言录入，28国语言秒译！这款智能鼠标硬核提升工作效率！', '//s2.jiguo.com/ec050e5a-764c-44cd-876b-409a6066f781/640?imageView2/1/w/520/h/320/q/100', ''),
(433, '「体验」生活迅速记录、旅行点滴随手画出，插画师用平板绘出绝美烟花！', '//s2.jiguo.com/42fcd9e4-3bff-41b9-b59a-fedb9f0380f4/640?imageView2/1/w/520/h/320/q/100', ''),
(432, '不止变焦！vivo X30 Pro这么玩，果小妞都馋哭了', '//s2.jiguo.com/16f4a8f1-aee2-4810-ab65-8628e66bd8c0/640?imageView2/1/w/520/h/320/q/100', ''),
(431, '5G的iPhone 12提前来了！“四筒”镜头叫板华为，外观设计炸翻老果粉......', '//s2.jiguo.com/c421b18d-a40a-4c93-b2e2-0ceb362525ec/640?imageView2/1/w/520/h/320/q/100', ''),
(430, '展望2020：夏普抢占智能生态圈，战略布局AIoT为美好生活加码', '//s2.jiguo.com/202664a2-2279-427d-a259-c28beb108cb8/640?imageView2/1/w/520/h/320/q/100', ''),
(429, '曾靠颜值吃遍天下网红手机鼻祖，比iPhone还火！如今靠四处抄袭骗人而活……', '//s2.jiguo.com/172affe6-63d3-4688-af8c-b1fa407ee861/640?imageView2/1/w/520/h/320/q/100', ''),
(427, '「体验」插画师为啥爱上用平板画画？华为MatePad Pro体验：比纸好用！', '//s2.jiguo.com/c83f597c-9f99-4db5-acab-40903d825d5f/640?imageView2/1/w/520/h/320/q/100', ''),
(428, '对标Model Y！蔚来第三款量产车型EC6发布', '//s2.jiguo.com/bf6a3b96-a157-4baf-a0e1-60af720cd735/640?imageView2/1/w/520/h/320/q/100', ''),
(426, '新宝骏发布首款HUAWEI HiCar智能汽车：疲劳检测还能远程控制家居', '//s2.jiguo.com/14e63650-4b55-46e4-9720-a6cce0ff827d/640?imageView2/1/w/520/h/320/q/100', ''),
(425, '地表最全5G“双模芯”大逃杀！一天狂跑烧光半年流量，最牛B居然是它......', '//s2.jiguo.com/068fae35-b1b0-4dfe-b422-17933d7b32a8/640?imageView2/1/w/520/h/320/q/100', ''),
(424, '「体验」一款解放你沉重背包的平板！随时随地，想画就画', '//s2.jiguo.com/a10f1564-5520-47ac-b0a3-00f78e73d6ed/640?imageView2/1/w/520/h/320/q/100', ''),
(423, '不止便携？首款“变形”鼠标发布背后：PC配件也要面向未来', '//s2.jiguo.com/c21f9714-c2b2-4b65-8a94-4964426c5bc8/640?imageView2/1/w/520/h/320/q/100', ''),
(421, '「体验」工作娱乐同屏看，外出写生“一机解决”！这款平板给你绘画新体验', '//s2.jiguo.com/70fe3749-5a41-4e8f-a3cd-d8d852a1d3a9/640?imageView2/1/w/520/h/320/q/100', ''),
(422, '特斯拉死敌现身！电动“坦克”风骚走位，性能秒杀超级跑车', '//s2.jiguo.com/71fe7f29-5900-4c68-b20a-0ccca211bc07/640?imageView2/1/w/520/h/320/q/100', ''),
(420, '「体验」一台机器就能解决加湿和净化？知乎大V硬核测试，效果惊人！', '//s2.jiguo.com/44730ecc-ccd2-449c-a25a-832c3298eaeb/640?imageView2/1/w/520/h/320/q/100', ''),
(419, '号称AirPods Pro的鼻祖！？苹果商店上架的限定款，这点太鸡肋但颜值妹子都爱了.....', '//s2.jiguo.com/ae2e0c56-a7bb-4579-9db7-be6cad52802b/640?imageView2/1/w/520/h/320/q/100', ''),
(418, '「体验」随手记录灵感点滴！价格亲民、携带方便，移动办公好选择！', '//s2.jiguo.com/ccf02d6b-0d1d-4d72-ac69-5a2d397a8e48/640?imageView2/1/w/520/h/320/q/100', ''),
(417, '等等党的胜利！苹果官网iPad 2019版大降价，2499元起', '//s2.jiguo.com/0a9892f2-c4a2-446a-9751-3205c93535a4/640?imageView2/1/w/520/h/320/q/100', ''),
(416, 'Sim卡迎30年最大创新？手机竟还能这么玩，安卓用户：真香！', '//s2.jiguo.com/84463d90-639d-4123-b0a2-0699763a64d7/640?imageView2/1/w/520/h/320/q/100', ''),
(415, '享受家庭温馨，尽情聚会娱乐，这5款好物能让你春节过得更舒适', '//s2.jiguo.com/8cfdd3b4-ecb4-47f6-bf02-d1770a625486/640?imageView2/1/w/520/h/320/q/100', ''),
(414, 'iPhone当成白菜卖！探访全球最大苹果“黑市”，暴利程度你绝对想不到......', '//s2.jiguo.com/1b279f03-dc58-4036-8c8f-8d84534a1128/640?imageView2/1/w/520/h/320/q/100', ''),
(413, '「体验」宝妈的空气质量改善神器！加湿净化自由组合，清洁维护方便随心！', '//s2.jiguo.com/cb9cf5fe-c064-41ed-95e2-89e74294210b/640?imageView2/1/w/520/h/320/q/100', ''),
(412, '「体验」轻便好用随身携带，这款平板让插画师工作带娃两不误！', '//s2.jiguo.com/1ad8ee98-3614-486c-8f16-c1385c8d867d/640?imageView2/1/w/520/h/320/q/100', ''),
(411, '价格战的头排兵!标压U竟然被打到4千以下,荣耀MagicBook Pro上手玩', '//s2.jiguo.com/6db1f693-6a59-43d8-aacb-3fe4a9b2c9b9/640?imageView2/1/w/520/h/320/q/100', ''),
(410, '不到30万？国产特斯拉降价了！友商看完有点慌……', '//s2.jiguo.com/e9412c89-4e6a-4d9d-94cd-baafd44d247d/640?imageView2/1/w/520/h/320/q/100', ''),
(408, '酒店水壶不干净，旅行只能喝凉水？口袋电热杯让你放心喝热水！', '//s2.jiguo.com/5318e77f-75a4-4b82-a57d-15907ed45c0f/640?imageView2/1/w/520/h/320/q/100', ''),
(409, '全球最快“陆地巡洋舰”！首列智能高铁来了，这是离我想象中2020最近的一次....', '//s2.jiguo.com/aff148d7-aebd-48f1-96eb-7a81eca247de/640?imageView2/1/w/520/h/320/q/100', ''),
(407, '「体验」加湿迅速，空气湿而不潮！这款加湿净化一体机是南方人的救星！', '//s2.jiguo.com/e5018631-b9c7-46a2-a996-a84fc5004c02/640?imageView2/1/w/520/h/320/q/100', ''),
(406, '千元换屏100块搞定！iPhone维修“黑料”太多，套路太深坑得老炮儿都哭了......', '//s2.jiguo.com/5794416b-fdbe-49ea-ab7e-5975ff399a4d/640?imageView2/1/w/520/h/320/q/100', ''),
(405, '「CES 2020」全世界最大的科技盛会！火爆的就连苹果都想凑上来参加......', '//s2.jiguo.com/a491bc5b-cc93-4c7b-830c-5b17de011930/640?imageView2/1/w/520/h/320/q/100', ''),
(403, '「CES 2020」世界首款！超大曲率满足所有幻想，三星曲面显示器CTG9/CTG7发布', '//s2.jiguo.com/eeeb9f20-b386-4bd4-94c5-68168446115e/640?imageView2/1/w/520/h/320/q/100', ''),
(404, '这届父母不再“难”！搞定孩子听说读写，这个趣萌AI机器人有妙招', '//s2.jiguo.com/0f834dec-be33-440f-8c60-334b7fd0b62d/640?imageView2/1/w/520/h/320/q/100', ''),
(402, '我们跑去南极，只为带Ta做一场终极“修行”', '//s2.jiguo.com/1ffdd8aa-b10a-458d-a0f5-105a2d396fad/640?imageView2/1/w/520/h/320/q/100', ''),
(401, '「CES 2020」只剩一块屏！三星“0边框”8K电视现场体验，这设计真的awesome……', '//s2.jiguo.com/6b0263de-c936-4cf0-a9ef-99832394b91f/640?imageView2/1/w/520/h/320/q/100', ''),
(399, '线上中国好声音？发语音弹幕飙高音，与女神合唱真的so easy！', '//s2.jiguo.com/dcc860c0-17b2-4d8e-9652-304164fc060f/640?imageView2/1/w/520/h/320/q/100', ''),
(400, '「CES 2020」iMac迎来最强对手？惠普发布多款新品，还有90%屏占比笔记本', '//s2.jiguo.com/719869b3-251c-4deb-be06-40db51784ba0/640?imageView2/1/w/520/h/320/q/100', ''),
(398, '「CES 2020」国产5G再出海！TCL 5G手机新品亮相CES，还有喵星人追踪器……', '//s2.jiguo.com/d6e7049d-b305-4029-b51a-a30382dde569/640?imageView2/1/w/520/h/320/q/100', ''),
(397, '「CES 2020」Win 10 新玩具：戴尔展出Alienware “UFO”原型机，设计激似Nintendo Switch', '//s2.jiguo.com/9ae438d6-60cd-40ab-b6db-4d05f13f2346/640?imageView2/1/w/520/h/320/q/100', ''),
(395, '性能神机vs狂野南极？极地求生竟然被搞成最虐心的终极修行', '//s2.jiguo.com/135b8c83-ba0e-4563-ad46-caf2060dfc69/640?imageView2/1/w/520/h/320/q/100', ''),
(396, '「CES 2020」续航能撑一整天？全球首款5G笔记本亮相CES', '//s2.jiguo.com/4d6c33b6-46c7-40e1-bd87-99664250278c/640?imageView2/1/w/520/h/320/q/100', ''),
(394, '2499元！realme 真我X50 5G发布，2020年第一台真香机来了', '//s2.jiguo.com/36d4116c-ff0f-4865-864a-8c4a7b414094/640?imageView2/1/w/520/h/320/q/100', ''),
(393, '真香预定！2020第一款新机动手玩！120Hz+双模5G只卖2499', '//s2.jiguo.com/f8468ba2-76ea-45d2-b31c-7945d65965e6/640?imageView2/1/w/520/h/320/q/100', ''),
(392, '中国速度真香！马斯克官宣国产Model Y，新能源车企迎来最大挑战', '//s2.jiguo.com/21de176e-36e5-4b85-9b37-7ab9b8ce2c91/640?imageView2/1/w/520/h/320/q/100', ''),
(390, '不会用你就亏大了！iPhone“隐藏核武器”原来是它，这份使用指南拿稳啦！', '//s2.jiguo.com/a8211b94-325c-44b0-b660-be4cee3b4ff2/640?imageView2/1/w/520/h/320/q/100', ''),
(391, '2019最佳剁手之选：盘点那些承包全年幸福感的尖货', '//s2.jiguo.com/e8f6013c-60cd-463b-b9af-dd60d28395b1/640?imageView2/1/w/520/h/320/q/100', ''),
(389, '「CES 2020」口袋里的徕卡！insta 360发布首款模块化运动相机，还能“坐”上无人机……', '//s2.jiguo.com/6253e600-5561-485c-8108-4876d3fd3cf9/640?imageView2/1/w/520/h/320/q/100', ''),
(388, '「CES 2020」L2级自动驾驶！小牛发布首款电动三轮摩托车，还有街头拉风神器', '//s2.jiguo.com/2ff1b56d-ad76-4f96-b74c-4149f0078082/640?imageView2/1/w/520/h/320/q/100', ''),
(387, '「体验」坐站智能记录，收纳简洁方便！这款工作站让你远离腰间盘突出！', '//s2.jiguo.com/4c144b5e-90d9-47a7-acd7-3bf5e46912fb/640?imageView2/1/w/520/h/320/q/100', ''),
(386, '5.5K视频内录、20张/秒连拍！佳能发布最强旗舰EOS 1D X Mark III', '//s2.jiguo.com/3c8192bf-1a90-45bc-a843-7e057623c5e0/640?imageView2/1/w/520/h/320/q/100', ''),
(385, '「体验」旅拍摄影师竟爱上用它拍大片？长焦堪比望远镜，人像镜头画质惊喜', '//s2.jiguo.com/5c04bede-1eef-410a-a452-bfbd5dd6756d/640?imageView2/1/w/520/h/320/q/100', ''),
(384, '「体验」最适合拍妹子的5G手机！50mm人像镜头加大光圈，怎么拍都好看！', '//s2.jiguo.com/ebb740f9-eb2a-44db-b42e-f8b1c9036bec/640?imageView2/1/w/520/h/320/q/100', ''),
(383, '「CES 2020」行走的头等舱！九号机器人发布年度“懒人神器”，还有更狂野的黑科技', '//s2.jiguo.com/9f0cedc5-3fb7-4f1c-b254-2bd2b2d1d88f/640?imageView2/1/w/520/h/320/q/100', ''),
(381, '专访杨元庆：常程离开不影响联想手机业务在中国开展，遇到好产品会积极进取', '//s2.jiguo.com/fe0772d4-f24c-4bc4-9a49-9fe87326aff9/640?imageView2/1/w/520/h/320/q/100', ''),
(382, '新宝骏付昊：HUAWEI HiCar首款汽车量产，2020年实现车载系统定制化', '//s2.jiguo.com/a037ed34-e555-4467-abff-4a048ab4be27/640?imageView2/1/w/520/h/320/q/100', ''),
(380, '「CES 2020」电致玻璃引爆手机圈！一加发布全新手机Concept One，首款“超跑”概念机', '//s2.jiguo.com/9e275201-17d9-4cfd-ae7a-588c6724284c/640?imageView2/1/w/520/h/320/q/100', ''),
(379, '「CES 2020」联想折叠屏电脑将开卖！售价2499美元，经过30000次折叠测试', '//s2.jiguo.com/0a397e08-2030-4f14-926a-9e1b8fcef385/640?imageView2/1/w/520/h/320/q/100', ''),
(378, '「CES 2020」索尼造车了！浑身都是“世界名牌”，真能挑战特斯拉吗？', '//s2.jiguo.com/c8856054-b9d9-4b5d-9f06-d157c96ff3b2/640?imageView2/1/w/520/h/320/q/100', ''),
(377, '「CES 2020」非黑即白不存在！海信全球首款彩色水墨屏，今年下半年开售', '//s2.jiguo.com/30167ce9-f31f-4908-9bbc-3328f8e6864d/640?imageView2/1/w/520/h/320/q/100', ''),
(376, '「CES 2020」未来座驾就是它？小牛新款拉风神车现场体验！自动驾驶只是前菜……', '//s2.jiguo.com/cd6400f6-beaf-430d-872a-348b4af2e69b/640?imageView2/1/w/520/h/320/q/100', ''),
(375, '「CES 2020」超逼真“人造人”、会做饭的机械臂！科技圈开年大战，三星的王炸是个“球”？', '//s2.jiguo.com/358b8bbe-aec1-43a1-a4a7-516a7d27dfe0/640?imageView2/1/w/520/h/320/q/100', ''),
(374, '手机4G秒变满血5G，还能续航翻倍！又被华为香哭了(附脑洞VLOG)', '//s2.jiguo.com/5bc8a2fd-ae49-43a4-bf57-4ab7d1ad0596/640?imageView2/1/w/520/h/320/q/100', ''),
(373, '「CES 2020」最吸睛神作！Moto razr被我们搞了一次脑洞狂玩，这条评测戏忒多！', '//s2.jiguo.com/71f361ca-5400-479d-9d0b-dfe55cd66d22/640?imageView2/1/w/520/h/320/q/100', ''),
(372, '「体验」细节完美记录，AI智能建议！普通人也能轻松用手机拍大片', '//s2.jiguo.com/c7e3dc05-ee33-46fc-9f19-e2c3c047758c/640?imageView2/1/w/520/h/320/q/100', ''),
(371, '「CES 2020」海信发布卷帘门激光电视！升降过分神奇，原来这才是柔性屏正确打开方式', '//s2.jiguo.com/5a2269e0-bf54-4e05-8d2a-3713de139636/640?imageView2/1/w/520/h/320/q/100', ''),
(370, '「CES 2020」投影这么会玩？会教育会做饭还指导健身，未来智慧家庭就这样', '//s2.jiguo.com/45aba8bb-77fb-4c1e-af64-619673ae7c6b/640?imageView2/1/w/520/h/320/q/100', ''),
(369, '年度难上加难的评测！我们邀三大安卓旗舰共赴南极....', '//s2.jiguo.com/13572d19-0cc3-42d2-8eb0-fcce073cac8c/640?imageView2/1/w/520/h/320/q/100', ''),
(368, '吹风、净化、加湿三合一！戴森推出2020开年新品，设计依然“戴森味儿”', '//s2.jiguo.com/9c72c128-1b43-4757-a9ce-3ef8437df908/640?imageView2/1/w/520/h/320/q/100', ''),
(367, '告别“我太难了”！这些好物帮你把周末过得快乐一点', '//s2.jiguo.com/992a190a-26e4-48e3-b246-69f303b7411f/640?imageView2/1/w/520/h/320/q/100', ''),
(366, '戴森新机竟同时颠覆三大行业？这样的魔法空气好想吸一辈子！', '//s2.jiguo.com/97f9ef03-52a3-4653-b18f-7b3b183ee8c9/640?imageView2/1/w/520/h/320/q/100', ''),
(365, '「CES 2020」秀肌肉发爆款！联想展台上的新玩意儿，多得数不过来......', '//s2.jiguo.com/ed915484-90eb-40b3-a26e-3e3d7580a892/640?imageView2/1/w/520/h/320/q/100', ''),
(364, '「CES」真香黑科技：当电视刷起抖音，就彻底没手机啥事了...', '//s2.jiguo.com/cc62e193-b2df-4232-8b1d-8d53a6b43cb1/640?imageView2/1/w/520/h/320/q/100', ''),
(363, '「体验」恋爱助攻一个打印机就够了！轻松连接即拍即打，定格美好瞬间！', '//s2.jiguo.com/7e796ee2-9e6f-4020-babd-7b0b22124e58/640?imageView2/1/w/520/h/320/q/100', ''),
(362, '「南极VLOG」三大旗舰挑战冰天雪地，性能依旧稳吗？', '//s2.jiguo.com/c8958afb-1c01-43db-8df4-cb0a2c4a9142/640?imageView2/1/w/520/h/320/q/100', ''),
(361, '新“信仰”游戏本！双屏笔记本的新玩法，幻影精灵OMEN X上手玩', '//s2.jiguo.com/d4ed7029-57b1-4057-b4a7-6906754b275c/640?imageView2/1/w/520/h/320/q/100', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
